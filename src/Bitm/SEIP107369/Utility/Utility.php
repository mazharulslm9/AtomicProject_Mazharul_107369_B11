<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Bitm\SEIP107369\Utility;

class Utility {

    public $message = "";

    static public function d($param = false) {
        echo "<pre>";
        var_dump($param);
        echo "</pre>";
    }

    static public function dd($param = false) {
        self::d($param);
        die();
    }

    static public function redirect($url = "/AtomicProject_Mazharul_107369_B11/views/SEIP107369/Book/index.php") {
        header("Location:" . $url);
    }

    static public function message($message = null) {
        if (is_null($message)) { // please give me message
            $_message = self::getMessage();
            return $_message;
        } else { //please set this message
            self::setMessage($message);
        }
    }

    static private function getMessage() {
        $_message = $_SESSION['message'];
        $_SESSION['message'] = "";
        return $_message;
    }

    static private function setMessage($message) {
        $_SESSION['message'] = $message;
    }

}
