<?php

namespace App\Bitm\SEIP107369\Hobby;

use App\Bitm\SEIP107369\Utility\Utility;

class HobbyNames {

    public $id = " ";
    public $title = " ";
    public $hobby = " ";
    public $created = " ";
    public $modified = " ";
    public $created_by = " ";
    public $modified_by = " ";
    public $deleted_at = " ";

    public function __construct($data = false) {
        if (is_array($data) && array_key_exists('id', $data) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        $this->hobby = $data['hobby'];
    }

    public function show($id = false) {
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        $lnk = mysql_select_db("new") or die("Cannot select database.");

        $query = "SELECT * FROM `hobbies_tbl` WHERE id=" . $id;
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function index() {
        $hobby = array();

        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        $lnk = mysql_select_db("new") or die("Cannot select database.");

        $query = "SELECT * FROM `hobbies_tbl`";
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $hobby[] = $row;
        }
        return $hobby;
    }

    public function create() {
        echo 'I am create form';
    }

    public function store() {
        $checkbox = $_POST['hobby'];
        $chk = " ";
        foreach ($checkbox as $chk1) {
            $chk .= $chk1 . ",";
        }
        $ch = trim($chk, ",");
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        $lnk = mysql_select_db("new") or die("Cannot select database.");

        $query = "INSERT INTO `new`.`hobbies_tbl` (`hobbies`) VALUES ('" . $ch . "');";

        $result = mysql_query($query);


        if ($result) {
            Utility::message("<strong>$ch</strong>  are added successfully.");
        } else {
            Utility::message("There is an error while saving data. Please try again later.");
        }

        Utility::redirect('index.php');
    }

    public function edit($id = false) {
        if (is_null($id)) {
            return;
        }
        $con = mysql_connect("localhost", "root")or die("Mysql not connected");
        $dd = mysql_select_db('new') or die("Cannot select database.");;
        $q = "SELECT * FROM `hobbies_tbl` where `id`='$id'";

        $query = mysql_query($q);

        $result = mysql_fetch_assoc($query);
//        Utility::dd($result);
        return $result;
    }

    public function update() {
        $checkbox = $_POST['hobby'];
        $chk = "";


        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        $lnk = mysql_select_db("new") or die("Cannot select database.");
        foreach ($checkbox as $chk1) {
            $chk .= $chk1 . ",";
            $query = "UPDATE `new`.`hobbies_tbl` SET `hobbies` = '" . trim($chk, ",") . "' WHERE `hobbies_tbl`.`id` = " . $this->id;
        }



        $result = mysql_query($query);

        if ($result) {
            Utility::message("Hobbies are  edited successfully.");
        } else {
            Utility::message("There is an error while saving data. Please try again later.");
        }

        Utility::redirect('index.php');
    }

    public function delete($id = NULL) {
        if (is_null($id)) {
            Utility::message("No Id Found!!");
            return Utility::redirect("index.php");
        }

        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        $lnk = mysql_select_db("new") or die("Cannot select database.");
        $query = "DELETE FROM `new`.`hobbies_tbl` WHERE `hobbies_tbl`.`id` = " . $id;
        $result = mysql_query($query);


        if ($result) {
            Utility::message("Hobbies deleted successfully.");
        } else {
            Utility::message("There is an error while deleting title. Please try again later.");
        }

        Utility::redirect('index.php');
    }

}
