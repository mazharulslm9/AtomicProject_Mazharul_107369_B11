<?php
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'AtomicProject_Mazharul_107369_B11' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'startup.php');

use App\Bitm\SEIP107369\TermsCondition\Terms;
use \App\Bitm\SEIP107369\Utility\Utility;

$new_terms = new Terms();
$term = $new_terms->show($_GET['id']);
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Terms & Condition</title>

        <!-- Bootstrap -->
        <link href="./../../../asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="./../../../asset/css/custom.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--font-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    </head>
    <body class="bg-primary">
        <section>
            <div class="container">
                <div class="row">
                    <h3 class="text-center">Update Terms & Condition</h3>
                    <hr>
                    <form class="form-inline text-center" action="update.php" method="post">
                        <div class="form-group">
                            <label for="exampleInputName2">Chose Terms & Condition: </label>
                            <input type="hidden" class="form-control"  name="id" id="exampleInputName2" value="<?php echo $term['id']; ?>" >
                             <input type="text" class="form-control"   id="exampleInputName2" value="<?php echo $term['terms']; ?>" >
                           
                            <label class="checkbox-inline">
                                <input type="checkbox" id="inlineCheckbox1" name="title" value="Agree"> Agree
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" id="inlineCheckbox2" name="title" value="Disagree"> Disagree
                            </label>
                        </div>

                        <button type="submit" class="btn btn-warning btn-md">Update</button>
                    </form>
                </div>
            </div>
        </section>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="./../../../asset/js/bootstrap.min.js"></script>

    </body>
</html>