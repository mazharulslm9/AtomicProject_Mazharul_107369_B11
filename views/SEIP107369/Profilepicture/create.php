<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Pofile Picture</title>

        <!-- Bootstrap -->
        <link href="../../../asset/css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-info">
        <div class="container">

            <h3 class="text-center text-success">Create Profile Picture</h3>
            <hr>
            <form class="form-inline" role="form" action="store.php" method="POST" enctype="multipart/form-data">
                <div class="form-group text-primary">

                    <label for="exampleInputName2">Username: </label>
                    <input type="text" name="name" class="form-control" >

                    <label for="exampleInputName2">Upload Picture: </label>
                    <input type="file" name="profile_pic" class="form-control" >
                </div>

                <button type="submit" name="submit" class="btn btn-success">Submit</button>
            </form>


        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../asset/js/bootstrap.min.js"></script>

    </body>
</html>