<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Pofile Picture</title>

        <!-- Bootstrap -->
        <link href="../../../asset/css/bootstrap.css" rel="stylesheet">
        <link href="../../../asset/css/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'AtomicProject_Mazharul_107369_B11' . DIRECTORY_SEPARATOR . "views" . DIRECTORY_SEPARATOR . "startup.php");

        use App\Bitm\SEIP107369\Profilepicture\Picture;
        use App\Bitm\SEIP107369\Utility\Utility;

$pp = new Picture();
        $pps = $pp->index();
        ?>
        <div class="container">
            <h3 class="text-center">Profile Picture List</h3>
            <hr>
            <div class="row">
                <div class="col-md-3">
                    <button  class="btn btn-info btn-md"> <a href="create.php">Add profile picture</a></button>
                </div><!-- /.col-lg-6 -->
                <div class="col-md-3">
                    <select style="width: 50%;color: black">
                        <option>10</option>
                        <option>20</option>
                        <option>30</option>
                        <option>40</option>
                        <option>50</option>
                    </select>
                </div>
                <div class="col-md-3">
                   <button  class="btn btn-danger btn-md">Download as PDF/XL</button>

                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Go!</button>
                        </span>
                    </div><!-- /input-group -->
                </div><!-- /.col-lg-6 -->
            </div><!-- /.row -->

            <hr>
            <div style="width: 50%;margin: 0 auto;margin: 10px;" class="row alert alert-success" id="message" >
                <?php echo Utility::message(); ?>


            </div>

            <table class="table table-bordered table-hover text-center bg-info">
                <thead >
                    <tr>
                        <th class="text-center">SL No</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Picture</th>
                        <th colspan="3" class="text-center">Action</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    $srl = 1;
                    foreach ($pps as $pic) {
                        ?>
                        <tr>
                            <td><?php echo $srl; ?></td>
<!--                            <td><?php echo $pic->id ?></td>-->
                            <td><?php echo $pic->name ?></td>
                            <td><img src="upload/<?php echo $pic->profile_pic; ?> " style="width: 20%;"/></td>
                            <td><a href="show.php?id=<?php echo $pic->id ?>" class="btn btn-success ">View</a></td>
                            <td><a href="edit.php?id=<?php echo $pic->id ?>" class="btn btn-primary ">Edit</a></td>
                            <td>
                                <form action="delete.php" method="POST">
                                    <input type="hidden" name="id" value="<?php echo $pic->id; ?>"/>
                                    <button class="btn btn-danger delete" type="submit" >Delete</button>
                                </form>
                            </td>
                        </tr>
                        <?php
                        $srl++;
                    }
                    ?>
                </tbody>
            </table>
            <a href="" onclick="window.history.go(-1)" class="pull-left "><button class="btn btn-danger">Back</button></a>
            <a href="./../../../index.php"  class="pull-right "><button class="btn btn-danger">Main List</button></a>
            <nav class="text-center">
                <ul class="pagination">
                    <li>
                        <a href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>
                        <a href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../asset/js/bootstrap.min.js"></script>
        <script>
                $('.delete').bind('click', function (e) {
                    var dlt = confirm("Are you sure to Delete Profile Picture");
                    if (!dlt) {
                        e.preventDefault();
                    }
                });
                $('#message').fadeOut(5000);
        </script>

    </body>
</html>